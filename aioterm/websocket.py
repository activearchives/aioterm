# based on: https://github.com/jupyter/terminado/blob/master/terminado/websocket.py
"""Tornado websocket handler to serve a terminal interface.
"""
# Copyright (c) Jupyter Development Team
# Copyright (c) 2014, Ramalingam Saravanan <sarava@sarava.net>
# Distributed under the terms of the Simplified BSD License.

# from __future__ import absolute_import, print_function

# from urllib.parse import urlparse

import json
import logging
import aiohttp
import asyncio


class TermSocket():
    """Multipurpose handler for a terminal, including the websocket"""

    def __init__(self, term_manager):
        self.term_manager = term_manager
        self.term_name = ""
        self.size = (None, None)
        self.terminal = None
        self._logger = logging.getLogger(__name__)
        # https://stackoverflow.com/questions/29475007/python-asyncio-reader-callback-and-coroutine-communication
        self.send_queue = asyncio.Queue()

    # was part of open
    #     # Jupyter has a mixin to ping websockets and keep connections through
    #     # proxies alive. Call super() to allow that to set up:
    #     super(TermSocket, self).open(url_component)

    async def handle(self, request, ws):
        # following https://github.com/aio-libs/aiohttp-demos/blob/master/demos/chat/aiohttpdemo_chat/views.py
        # alt you can also catch web.HTTPException on prepare
        self.ws = ws
        await ws.prepare(request)
        name = request.match_info.get("name", "")
        self._logger.info("TermSocket.open: %s", name)
        # url_component = _cast_unicode(url_component)
        self.term_name = name or 'tty'
        self.terminal = self.term_manager.get_terminal(name)
        self.terminal.clients.append(self)
        # start the send_messages loop
        self.send_messages_task = asyncio.create_task(self.send_messages())
        self.send_json_message(["setup", {}])
        self._logger.info("TermSocket.open: Opened %s", self.term_name)
        # Now drain the preopen buffer, if it exists.
        buffered = ""
        while True:
            if not self.terminal.preopen_buffer:
                break
            s = self.terminal.preopen_buffer.popleft()
            buffered += s
        if buffered:
            self.on_pty_read(buffered)

        async for msg in ws:
            if msg.type == aiohttp.WSMsgType.TEXT:
                command = json.loads(msg.data)
                ##logging.info("TermSocket.on_message: %s - (%s) %s", self.term_name, msg.type, len(msg.data) if isinstance(message, bytes) else message[:250])
                msg_type = command[0]

                if msg_type == "stdin":
                    self.terminal.ptyproc.write(command[1])
                elif msg_type == "set_size":
                    self.size = command[1:3]
                    self.terminal.resize_to_smallest()

        return ws

    def on_pty_read(self, text):
        """Data read from pty; send to frontend"""
        self.send_json_message(['stdout', text])

    def send_json_message (self, content):
        """ this fn is sync due to it's way of being called from TermManagerBase...
        thus the use of create_task... but this seems wrong
        ideally this call could/should be async? """
        json_msg = json.dumps(content)
        asyncio.create_task(self.send_queue.put(json_msg))

    async def send_messages (self):
        while 1:
            msg = await self.send_queue.get()
            await self.ws.send_str(msg)

    # async def send_json_message(self, content):
    #     json_msg = json.dumps(content)
    #     # self.write_message(json_msg)
    #     await self.ws.send_str(json_msg)

    # def on_close(self):
    #     """Handle websocket closing.

    #     Disconnect from our terminal, and tell the terminal manager we're
    #     disconnecting.
    #     """
    #     self._logger.info("Websocket closed")
    #     if self.terminal:
    #         self.terminal.clients.remove(self)
    #         self.terminal.resize_to_smallest()
    #     self.term_manager.client_disconnected(self)

    def on_pty_died(self):
        """Terminal closed: tell the frontend, and close the socket.
        Called by Manager
        """
        # also need to call this with await
        self.send_json_message(['disconnect', 1])
        # self.close()
        self.ws.close()
        self.terminal = None

