import sys, argparse
import aiohttp
from .handler import TermHandler
# from .websocket import TermSocket
from .managers import (TermManagerBase, SingleTermManager, UniqueTermManager)
import logging


ap = argparse.ArgumentParser("")
ap.add_argument("--host", default="localhost")
ap.add_argument("--port", type=int, default=8000)
ap.add_argument("--debug", action="store_true", default=False, help="sets logging level to DEBUG")
ap.add_argument("--verbose", action="store_true", default=False, help="sets logging level to INFO")
args = ap.parse_args()

# if sys.platform == 'win32':
#     # Folowing: https://docs.python.org/3/library/asyncio-subprocess.html
#     loop = asyncio.ProactorEventLoop()
#     asyncio.set_event_loop(loop)

if args.debug:
    logging.basicConfig(level=logging.DEBUG)
elif args.verbose:
    logging.basicConfig(level=logging.INFO)

logging.info("Starting test server...")
app = aiohttp.web.Application()
term_manager = SingleTermManager(shell_command=['bash'])
# tsock = TermSocket(term_manager=term_manager)
term_handler = TermHandler(term_manager=term_manager)
app.router.add_route("GET", "/{name:.*}", term_handler.handler)
aiohttp.web.run_app(app, host=args.host, port=args.port)
