Adaptation of [terminado](https://github.com/jupyter/terminado) from the jupyter project (Ramalingam Saravanan and the Jupyter development team) to use asyncio/aiohttp instead of tornado. As an adaptation, it carries existing dependencies:

* [ptyprocess](https://github.com/pexpect/ptyprocess) part of the pexpect project, which seems to be an elaboration of the standard python library pty providing enhanced support for pseudo-terminals across different operating systems.
* [term.js](https://github.com/chjj/term.js) Javascript (from Fabrice Bellard via Christopher Jeffrey) that does the all important translation of terminal character-based io to a web page based HTML context. Currently this dependency is hard coded in, via the file [term.js](aioterm/data/term.js) but as attempts seem to be underway to make this project [more modular and maintained](https://github.com/xtermjs/xterm.js) (in the javascript ecosystem sense), it may be interesting/important to have a parallel javascript build process for it based on this project in the future.


Usage
------------------

To see it working...

    python3 -m aioterm


To incorporate into an aiohttp project:

```python
from aioterm import SingleTermManager, TermSocket

HOSTNAME = "localhost"
PORT = 8000

app = web.Application()
term_manager = SingleTermManager(shell_command=['bash'])
tsock = TermSocket(term_manager=term_manager)
app.router.add_route("GET", "/{name:.*}", tsock.handler)

print (f"listening on http://{HOSTNAME}:{PORT}/")
web.run_app(app, host=HOSTNAME, port=PORT)
```

Notes
------
TermSock.handler is a [variable resource](https://docs.aiohttp.org/en/stable/web_quickstart.html#variable-resources) that expects an *optional* name in the match_info. This allows a single handler to serve multiple purposes:
* When name is empty, serve the initial static terminal HTML page
* When name is empty and a websocket connection is initiated, serve the websocket
* When name is term.js, serve the term.js static file

TermSock can be given custom paths for the index html (index) and term.js (termjs). The defaults are to files in aioterm/data.


Notes from the adaptation from Tornado to asyncio
---------------------------------------------------

WARNING: As an avid user of the jupyter project, and a programmer making use of asyncio/aiohttp, I really wanted to be able to have similar interactive terminal sessions in my aiohttp-based code. Not being in expert in either asyncio or the lower level workings of terminals, I am perhaps not the ideal developer to make this adaptation. This project is thus born more from need and desire than technical prowess and know how, and as such perhaps (initially) just a hacky proof of concept.

Besides the shift of using async and await instead of Tornado generators, the key adaptation occurs in the TermManagerBase with calls to asyncio.add_reader and asyncio.remove_reader. While it seems that it might be better to use a higher level API call (streams, transport/protocol), this method was the most direct adaptation of the Tornado code (which made use of Tornado's ioloop.add_handler). Additionally, it would seem a "natural" thing to do would be to use part of asyncio.subprocess. However, as the jupyter code relies on ptyprocess, itself based on pty, the lower level asyncio.add_reader API seems the best fit (as we basically have file descriptors to work with and otherwise synchronous code).

Another potential issue in the adaptation: as TermSocket.on_pty_read is called by the synchronous callback function TermManagerBase.pty_read (itself the part that gets attached to the asyncio event loop with add_reader), this function (TermSocket.on_pty_read) is also synchronous. As however its main purpose is to relay what it reads to the websocket(s) connected, it calls asyncio.add_task. In adapting the code, an asyncio.Queue was added thinking that this might be important to not allow this become a performance bottleneck (but I'm not 100% sure about this, testing would be important).

